$(document).ready(function() {
    function setHeight() {
      windowHeight = $(window).innerHeight();
      $('.main-banner iframe').css('min-height', windowHeight -140);
    };
    setHeight();
    
    $(window).resize(function() {
      setHeight();
    });

});

function openNav() {
    document.getElementById("mySidenav").style.transform = "translateX(0%)";
}
  
function closeNav() {
    document.getElementById("mySidenav").style.transform = "translateX(-100%)";
}