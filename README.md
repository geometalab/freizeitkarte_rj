# Freizeitkarte Rapperswil-Jona

## Beschreibung
Das ist eine Karte auf dem alle Brunnen, Bänke, Spielplätze und Grillstellen in Rapperswil-Jona eingezeichnet sind. Die Karte selbst ist mit uMap gemacht. Bearbeite werden kann die Karte hier: [Link](https://umap.osm.ch/de/map/anonymous-edit/7791:vRZYkvfqQQA7X6Xjid5cDt8pWcwve3CLGNTsCakxTbg).

## Nutzung
Die Webseite soll ein Beispiel sein wie man seine eigene Webkarte erstellen kann. Man kann das Repo clonen, das Iframe anpassen und allfällige Mail-Adressen und Links im html anpassen.
Auf [uMap](https://umap.osm.ch/de/) gibt es weitere Anleitungen wie man Karten erstellen kann.